const models = require("../models");
const bcrypt = require("bcrypt");
const moment = require("moment");
const jwt = require("jsonwebtoken");

const login = async (phone, password) => {
  try {
    let user = await findPhone(phone);
    if (!user) {
      return {
        messages: "Phone not found",
        code: 401
      }
    } else {
      const pwd = user.password;
      const { phone, id } = user;
      const match = await bcrypt.compare(password, pwd);
      if (match) {
        let input = {
          user: phone,
          id,
          expiredAt: moment().add(24, "hour").toDate(),
        };
        let token = jwt.sign(input, "HS256");
        return token
      }
      return {
        messages: "Wrong password",
        code: 401
      }
    }

  } catch (error) {
    return {
      error
    }
  }
}

const findPhone = async (phone) => {
  try {
    const data = await models.User.findOne({
      where: {
        phone
      }
    })
    return data;
  } catch (error) {
    return {
      error
    }
  }
}

const register = async (phone, password) => {
  try {
    let user = await findPhone(phone);
    if (user) {
      return {
        messages: "Phone already exist!",
        code: 401
      }
    }
    const saltRound = 10;
    bcrypt.genSalt(saltRound, (err, salt) => {
      bcrypt.hash(password, salt, async (err, hassPassword) => {
        if (err) throw err;
        return await models.User.create({
          phone,
          password: hassPassword,
        });

      });
    });

  } catch (error) {
    return {
      error
    }
  }
}

const getUserByToken = async (token) => {
  try {
    const tokenServer = "HS256";
    const decoded = await jwt.verify(token, tokenServer);
    return decoded;
  } catch (error) {
    return {
      error
    }
  }
}

module.exports = {
  login,
  register,
  getUserByToken
}