const models = require("../models");
const jwt = require("jsonwebtoken");

const insertInfo = async (name, date_of_birth, phone, address, site_url) => {
  try {
    return await models.Register_user.create({ name, date_of_birth, phone, address, status:1, site_url, contact_user: null, contact_time: null, result_note: null, document_status: null })
  } catch (error) {
    return {
      error
    }
  }
}

const getAllInfo = async (limit, offset) => {
  try {
    return await models.Register_user.findAndCountAll({
      limit,
      offset
    });
  } catch (error) {
    return {
      error
    }
  }
}

const getDetailsInfo = async (id) => {
  try {
    return await models.Register_user.findOne({
      where: { id }
    })
  } catch (error) {
    return {
      error
    }
  }
}

const changeStatus = async (id, status, result_note = "", token) => {
  try {
    const tokenServer = "HS256";
    const decoded = await jwt.verify(token, tokenServer);
    const contact_time = new Date().toString();
    return await models.Register_user.update({ status, contact_user: decoded.id, contact_time, result_note }, {
      where: {
        id
      }
    })
  } catch (error) {
    return {
      error
    }
  }
}

const findByStatus = async (status, limit, offset) => {
  try {
    return await models.Register_user.findAndCountAll({
      limit,
      offset,
      where: {
        status
      }
    })
  } catch (error) {
    return {
      error
    }
  }
}
module.exports = {
  insertInfo,
  getAllInfo,
  getDetailsInfo,
  changeStatus,
  findByStatus
}