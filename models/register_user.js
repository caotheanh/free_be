'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Register_user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Register_user.init({
    site_url: DataTypes.STRING,
    name: DataTypes.STRING,
    date_of_birth: DataTypes.STRING,
    phone: DataTypes.STRING,
    address: DataTypes.STRING,
    status: DataTypes.INTEGER,
    contact_user: DataTypes.INTEGER,
    contact_time: DataTypes.STRING,
    result_note: DataTypes.TEXT,
    document_status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Register_user',
  });
  return Register_user;
};