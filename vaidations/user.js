const { body } = require('express-validator');

let validateUser = () => {
  return [
    body('phone', 'Invalid does not Empty').notEmpty(),
    body('password', 'password more than 6 degits').isLength({ min: 6 }).notEmpty()
  ];
}
module.exports = {
  validateUser
}