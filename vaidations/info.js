const { body } = require('express-validator');

let validate = () => {
  return [
    body('name', 'Name does not Empty').notEmpty(),
    body('phone', 'Phone does not Empty').notEmpty(),
    body('date_of_birth','Date of birth does not Empty').notEmpty(),
    body('address', 'Address does not Empty').notEmpty(),
  ];
}
module.exports = {
  validate
}