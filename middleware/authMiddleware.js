const jwt = require('jsonwebtoken');
let isAuth = async (req, res, next) => {
  try {
    const tokenServer = "HS256";
    const token = req.body.token || req.query.token || req.headers["x-access-token"] || req.headers["token"];
    const decoded = await jwt.verify(token, tokenServer);
    if (decoded) {
      req.jwtDecoded = decoded;
      next();
    }
  } catch (error) {
    return res.json({
      nessages: "authorization",
    });
  }
};
module.exports = {
  isAuth,
};
