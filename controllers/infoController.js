const { validationResult } = require('express-validator');
const infoService = require("../services/infoServicre");

const getAllInfo = async (req, res) => {
  const { page, size } = req.query;
  const limit = size ? +size : 10;
  const offset = page ? +((page - 1) * limit) : 0;
  const data = await infoService.getAllInfo(limit, offset)
  return res.json({
    data
  })
}

const insertInfo = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { name, date_of_birth, phone, address,site_url } = req.body;
  const data = await infoService.insertInfo(name, date_of_birth, phone, address,site_url);
  return res.json({
    data
  })
}

const getDetails = async (req, res) => {
  const { id } = req.params;
  const data = await infoService.getDetailsInfo(id);
  return res.json({
    data
  })
}

const updateStatus = async (req, res) => {
  const { id, status, result_note } = req.body;
  const { token } = req.headers;
  const data = await infoService.changeStatus(id, status, result_note, token)
  return res.json({
    data
  })
}

const findByStatus = async (req, res) => {
  const { status, page, size } = req.query;
  const limit = size ? +size : 10;
  const offset = page ? +((page - 1) * limit) : 0;
  const data = await infoService.findByStatus(status, limit, offset)
  return res.json({
    data
  })
}

module.exports = {
  getAllInfo,
  insertInfo,
  getDetails,
  updateStatus,
  findByStatus
}