const userSerive = require("../services/userService");
var { validationResult } = require('express-validator');

const loginUser = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { phone, password } = req.body;

  const data = await userSerive.login(phone, password)
  return res.json({
    data
  })
}

const register = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    return;
  }
  const { phone, password } = req.body;
  const data = userSerive.register(phone, password);
  return res.json({ data })

}

const verifyUser = async (req, res) => {
  const { token } = req.body || req.params || req.query || req.headers['x-access-token'];
  const user = await userSerive.getUserByToken(token);
  return res.json({
    data: user
  })
}

module.exports = {
  loginUser,
  register,
  verifyUser
}