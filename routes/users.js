var express = require('express');
var router = express.Router();
const userController = require("../controllers/userController");
const validate = require("../vaidations/user");
const {isAuth} =  require("../middleware/authMiddleware");
/* GET users listing. */
router.post('/login', validate.validateUser(), userController.loginUser)

router.post("/register/data", validate.validateUser(), userController.register)

router.post("/verify-token",isAuth, userController.verifyUser)

module.exports = router;
