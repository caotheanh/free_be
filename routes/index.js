var express = require('express');
var router = express.Router();
const infoController = require("../controllers/infoController");
const { isAuth } = require("../middleware/authMiddleware");
const validate = require("../vaidations/info");
/* GET home page. */
router.get('/',infoController.getAllInfo )

router.post("/add",validate.validate(),infoController.insertInfo)

router.get("/detail/:id",infoController.getDetails)

router.put("/update",infoController.updateStatus)

router.get("/status",infoController.findByStatus)

module.exports = router;
